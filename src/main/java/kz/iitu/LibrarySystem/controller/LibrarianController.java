package kz.iitu.LibrarySystem.controller;
import io.swagger.annotations.*;
import kz.iitu.LibrarySystem.entity.Librarian;
import kz.iitu.LibrarySystem.service.LibrarianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@Api(value="Librarian controller class", description = "Operations pertaining to librarian in Library System")
public class LibrarianController {
    @Autowired
    private LibrarianService librarianService;

    @ApiOperation(value = "Add a librarian")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Librarian successfully stored in database table"),
            @ApiResponse(code = 401, message = "You are not authorized to add a new librarian"),
            @ApiResponse(code = 403, message = "Accessing to add a new librarian is forbidden"),
            @ApiResponse(code = 404, message = "The librarian is not found to add in database")
    }
    )
    @PostMapping("/admin/create/librarian")
    public void createLibrarian(
            @ApiParam(value = "Librarian object store in database table", required = true) @Valid @RequestBody Librarian librarian)  {
        librarianService.createLibrarian(librarian);
    }

    @ApiOperation(value = "View a list of all librarians", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of all librarians"),
            @ApiResponse(code = 401, message = "You are not authorized to view the list of all librarians"),
            @ApiResponse(code = 403, message = "Accessing the list of all librarians is forbidden"),
            @ApiResponse(code = 404, message = "The list of all librarians is not found")
    }
    )
    @GetMapping("/find/librarians")
    public List<Librarian> getAllLibrarians() {
        return librarianService.getAllLibrarians();
    }

    @ApiOperation(value = "Delete a librarian")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Librarian successfully deleted from table"),
            @ApiResponse(code = 401, message = "You are not authorized to delete librarian"),
            @ApiResponse(code = 403, message = "Accessing to delete librarian is forbidden"),
            @ApiResponse(code = 404, message = "The librarian is not found to delete")
    }
    )
    @DeleteMapping("/admin/delete/librarian/{id}")
    public Map<String, Boolean> deleteLibrarian(
            @ApiParam(value = "Librarian id from which librarian object will delete from database table", required = true) @PathVariable(value = "id") Long id) {
        return librarianService.deleteLibrarian(id);
    }
}

package kz.iitu.LibrarySystem.controller;
import io.swagger.annotations.*;
import kz.iitu.LibrarySystem.entity.Genre;
import kz.iitu.LibrarySystem.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@Api(value="Genre controller class", description = "Operations pertaining to genre in Library System")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @ApiOperation(value = "Add a genre")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Genre successfully stored in database table"),
            @ApiResponse(code = 401, message = "You are not authorized to add a new genre"),
            @ApiResponse(code = 403, message = "Accessing to add a new genre is forbidden"),
            @ApiResponse(code = 404, message = "The genre is not found to add in database")
    }
    )
    @PostMapping("/admin/create/genres")
    public void createGenre(
            @ApiParam(value = "Genre object store in database table", required = true) @Valid @RequestBody Genre genre)  {
        genreService.createGenre(genre);
    }

    @ApiOperation(value = "View a list of all genres", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of all genres"),
            @ApiResponse(code = 401, message = "You are not authorized to view the list of all genres"),
            @ApiResponse(code = 403, message = "Accessing the list of all genres is forbidden"),
            @ApiResponse(code = 404, message = "The list of all genres is not found")
    }
    )
    @GetMapping("/genres")
    public List<Genre> getAllGenres() {
        return genreService.getAllGenres();
    }

    @ApiOperation(value = "Get a genre by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find genre by id"),
            @ApiResponse(code = 401, message = "You are not authorized to find genre by id"),
            @ApiResponse(code = 403, message = "Finding genre by id is forbidden"),
            @ApiResponse(code = 404, message = "Finding genre by id is not found")
    }
    )
    @GetMapping("/find/genres/{id}")
    public Genre showGenreById(
            @ApiParam(value = "Genre id from which genre object will retrieve", required = true) @PathVariable(value = "id") Long id) {
        return genreService.showGenreById(id);
    }

    @ApiOperation(value = "Delete a genre")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Genre successfully deleted from table"),
            @ApiResponse(code = 401, message = "You are not authorized to delete genre"),
            @ApiResponse(code = 403, message = "Accessing to delete genre is forbidden"),
            @ApiResponse(code = 404, message = "The genre is not found to delete")
    }
    )
    @DeleteMapping("/admin/delete/genres/{id}")
    public Map<String, Boolean> deleteGenre(
            @ApiParam(value = "Genre id from which genre object will delete from database table", required = true) @PathVariable(value = "id") Long id) {
        return genreService.deleteGenre(id);
    }
}

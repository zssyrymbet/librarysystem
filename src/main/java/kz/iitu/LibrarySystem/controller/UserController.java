package kz.iitu.LibrarySystem.controller;
import io.swagger.annotations.*;
import kz.iitu.LibrarySystem.entity.User;
import kz.iitu.LibrarySystem.service.SecurityService;
import kz.iitu.LibrarySystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@Api(value="User controller class", description = "Operations pertaining to user in Library System")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @ApiOperation(value = "Add a user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User successfully stored in database table"),
            @ApiResponse(code = 401, message = "You are not authorized to add a new user"),
            @ApiResponse(code = 403, message = "Accessing to add a new user is forbidden"),
            @ApiResponse(code = 404, message = "The user is not found to add in database")
    }
    )
    @RequestMapping(value = "/user/registration", method = RequestMethod.POST)
    public String registration(
        @ApiParam(value = "User object store in database table", required = true) @Valid @RequestBody User userForm,
        BindingResult bindingResult)  {
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.createUser(userForm);
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User successfully registered"),
            @ApiResponse(code = 401, message = "You are not registered"),
            @ApiResponse(code = 403, message = "Accessing is forbidden"),
            @ApiResponse(code = 404, message = "The registered user is not found")
    }
    )
    public String welcome(Model model) {
        return "welcome";
    }

    @ApiOperation(value = "View a list of all users", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of all users"),
            @ApiResponse(code = 401, message = "You are not authorized to view the list of all user"),
            @ApiResponse(code = 403, message = "Accessing the list of all users is forbidden"),
            @ApiResponse(code = 404, message = "The list of all users is not found")
    }
    )
    @GetMapping("/find/users")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @ApiOperation(value = "Update a user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated user"),
            @ApiResponse(code = 401, message = "You are not authorized to update user"),
            @ApiResponse(code = 403, message = "Update user is forbidden"),
            @ApiResponse(code = 404, message = "Update user is not found")
    }
    )
    @PutMapping("/admin/update/users/{id}")
    public void updateUser(
            @ApiParam(value = "User id to update user object", required = true) @PathVariable(value = "id") Long id,
            @ApiParam(value = "Update user object", required = true) @Valid @RequestBody User user) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("authentication.getName() = " + authentication.getName());
        userService.updateUser(id, user);
    }

    @ApiOperation(value = "Get a user by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find user by id"),
            @ApiResponse(code = 401, message = "You are not authorized to find user by id"),
            @ApiResponse(code = 403, message = "Finding user by id is forbidden"),
            @ApiResponse(code = 404, message = "Finding user by id is not found")
    }
    )
    @GetMapping("/find/users/{id}")
    public User findById(
            @ApiParam(value = "User id from which user object will retrieve", required = true) @PathVariable(value = "username") Long id) {
        return userService.findById(id);
    }

    @ApiOperation(value = "Get a user by username")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find user by username"),
            @ApiResponse(code = 401, message = "You are not authorized to find user by username"),
            @ApiResponse(code = 403, message = "Finding user by username is forbidden"),
            @ApiResponse(code = 404, message = "Finding user by username is not found")
    }
    )
    @GetMapping("/find/users/{username}")
    public User findByUsername(
            @ApiParam(value = "User username from which user object will retrieve", required = true) @PathVariable(value = "username") String username) {
        return userService.findByUsername(username);
    }

    @ApiOperation(value = "Delete a user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User successfully deleted from table"),
            @ApiResponse(code = 401, message = "You are not authorized to delete user"),
            @ApiResponse(code = 403, message = "Accessing to delete user is forbidden"),
            @ApiResponse(code = 404, message = "The user is not found to delete")
    }
    )
    @DeleteMapping("/admin/delete/users/{id}")
    public Map<String, Boolean> deleteUser(
            @ApiParam(value = "User id from which user object will delete from database table", required = true) @PathVariable(value = "id") Long id) {
        return userService.deleteUser(id);
    }

}

package kz.iitu.LibrarySystem.controller;
import io.swagger.annotations.*;
import kz.iitu.LibrarySystem.entity.BorrowedBook;
import kz.iitu.LibrarySystem.entity.LibraryReader;
import kz.iitu.LibrarySystem.entity.Status;
import kz.iitu.LibrarySystem.notification.BorrowedBookNotifyReturnDateEvent;
import kz.iitu.LibrarySystem.service.BorrowedBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@Api(value="Borrowed Book controller class", description = "Operations pertaining to borrowed book in Library System")
public class BorrowedBookController implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private BorrowedBookService bookService;

    @ApiOperation(value = "Add a borrowed book")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Borrowed book successfully stored in database table"),
            @ApiResponse(code = 401, message = "You are not authorized to add a new borrowed book"),
            @ApiResponse(code = 403, message = "Accessing to add a new borrowed book is forbidden"),
            @ApiResponse(code = 404, message = "The borrowed book is not found to add in database")
    }
    )
    @PostMapping("/librarian/create/borrowed_books")
    public void createBook(
            @ApiParam(value = "Borrowed book object store in database table", required = true) @Valid @RequestBody BorrowedBook book)  {
        bookService.createBook(book);
        this.eventPublisher.publishEvent(new BorrowedBookNotifyReturnDateEvent(this, book.getId(),book.getIssue_date(), book.getReturn_date()));
        notifyUser();
        System.out.println("ISSUE DATE: " + book.getIssue_date());
        System.out.println("RETURN DATE: " + book.getReturn_date());
    }

    @ApiOperation(value = "View a list of all borrowed books", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of all borrowed books"),
            @ApiResponse(code = 401, message = "You are not authorized to view the list of all borrowed books"),
            @ApiResponse(code = 403, message = "Accessing the list of all borrowed books is forbidden"),
            @ApiResponse(code = 404, message = "The list of all borrowed books is not found")
    }
    )
    @GetMapping("/reader/borrowed_books")
    public List<BorrowedBook> getAllBorrowedBooks(
            @ApiParam(value = "Borrowed book libraryReader from which borrowed book object will retrieve", required = true) @PathVariable(value = "libraryReader") LibraryReader libraryReader) {
        return bookService.getAllBorrowedBooks(libraryReader);
    }

    @ApiOperation(value = "Get a borrowed book by status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find borrowed book by status"),
            @ApiResponse(code = 401, message = "You are not authorized to find borrowed book by status"),
            @ApiResponse(code = 403, message = "Finding borrowed book by status is forbidden"),
            @ApiResponse(code = 404, message = "Finding borrowed book by status is not found")
    }
    )
    @GetMapping("/librarian/borrowed_books/{status}")
    public List<BorrowedBook> findAllByStatus(
            @ApiParam(value = "Borrowed book status from which borrowed book object will retrieve", required = true) @PathVariable(value = "status") Status status) {
        return bookService.findAllByStatus(status);
    }

    @ApiOperation(value = "Get a borrowed book by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find borrowed book by id"),
            @ApiResponse(code = 401, message = "You are not authorized to find borrowed book by id"),
            @ApiResponse(code = 403, message = "Finding borrowed book by id is forbidden"),
            @ApiResponse(code = 404, message = "Finding borrowed book by id is not found")
    }
    )
    @GetMapping("/librarian/borrowed_books/{id}")
    public BorrowedBook getBookById(
            @ApiParam(value = "Borrowed book id from which borrowed book object will retrieve", required = true) @PathVariable(value = "id") Long id) {
        return bookService.getBookById(id);
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }

    @ApiOperation(value = "Update a borrowed book status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated status of borrowed book"),
            @ApiResponse(code = 401, message = "You are not authorized to update borrowed book status"),
            @ApiResponse(code = 403, message = "Update status of borrowed book is forbidden"),
            @ApiResponse(code = 404, message = "Update status of borrowed book is not found")
    }
    )
    @PatchMapping("/admin/update/borrowed_books/{id}")
    public void updateBook(
            @ApiParam(value = "Borrowed book id to update borrowed book object", required = true) @PathVariable(value = "id") Long id,
            @ApiParam(value = "Update borrowed book object", required = true) @Valid @RequestBody Status status) {
        bookService.updateBook(id, status);
        System.out.println("Book was updated!");
    }

    public void notifyUser(){
        System.out.println("Notification:");
    }

    @ApiOperation(value = "Delete a borrowed book")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Borrowed book successfully deleted from table"),
            @ApiResponse(code = 401, message = "You are not authorized to delete borrowed book"),
            @ApiResponse(code = 403, message = "Accessing to delete borrowed book is forbidden"),
            @ApiResponse(code = 404, message = "The borrowed book is not found to delete")
    }
    )
    @DeleteMapping("/admin/delete/borrowed_books/{id}")
    public Map<String, Boolean> deleteBook(
            @ApiParam(value = "Borrowed book id from which borrowed book object will delete from database table", required = true) @PathVariable(value = "id") Long id) {
        return bookService.deleteBorrowedBook(id);
    }
}

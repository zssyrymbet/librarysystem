package kz.iitu.LibrarySystem.controller;
import io.swagger.annotations.*;
import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@Api(value="Author controller class", description = "Operations pertaining to author in Library System")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @ApiOperation(value = "Add an author")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Author successfully stored in database table"),
            @ApiResponse(code = 401, message = "You are not authorized to add a new author"),
            @ApiResponse(code = 403, message = "Accessing to add a new author is forbidden"),
            @ApiResponse(code = 404, message = "The author is not found to add in database")
    }
    )
    @PostMapping("/admin/create/author")
    public void createAuthor(
            @ApiParam(value = "Author object store in database table", required = true) @Valid @RequestBody Author author)  {
        authorService.createAuthor(author);
    }

    @ApiOperation(value = "View a list of all authors", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of all authors"),
            @ApiResponse(code = 401, message = "You are not authorized to view the list of all authors"),
            @ApiResponse(code = 403, message = "Accessing the list of all authors is forbidden"),
            @ApiResponse(code = 404, message = "The list of all authors is not found")
    }
    )
    @GetMapping("/authors")
    public List<Author> getAllAuthors() {
        return authorService.getAllAuthors();
    }

    @ApiOperation(value = "Get an author by Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find author by id"),
            @ApiResponse(code = 401, message = "You are not authorized to find author by id"),
            @ApiResponse(code = 403, message = "Finding author by id is forbidden"),
            @ApiResponse(code = 404, message = "Finding author by id is not found")
    }
    )
    @GetMapping("/find/authors/{id}")
    public Author getAuthorById(
            @ApiParam(value = "Author id from which author object will retrieve", required = true) @PathVariable(value = "id") Long id) {
        return authorService.getAuthorById(id);
    }

    @ApiOperation(value = "Get an author by name and surname")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Author successfully find by name and surname"),
            @ApiResponse(code = 401, message = "You are not authorized to find author by name and surname"),
            @ApiResponse(code = 403, message = "Finding author by name and surname is forbidden"),
            @ApiResponse(code = 404, message = "Finding author by name and surname is not found")
    }
    )
    @GetMapping("/authors/find/")
    public Author getAuthor(
        @ApiParam(value = "Author name from which author object will retrieve", required = true) @RequestParam("name") String name,
        @ApiParam(value = "Author surname from which author object will retrieve", required = true) @RequestParam("surname") String surname) {
            System.out.println(name + " " + surname);
        return authorService.getAuthor(name,surname);
    }

    @ApiOperation(value = "Delete an author")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Author successfully deleted from table"),
            @ApiResponse(code = 401, message = "You are not authorized to delete author"),
            @ApiResponse(code = 403, message = "Accessing to delete author is forbidden"),
            @ApiResponse(code = 404, message = "The author is not found to delete")
    }
    )
    @DeleteMapping("/admin/delete/authors/{id}")
    public Map<String, Boolean> deleteAuthor(
            @ApiParam(value = "Author Id from which author object will delete from database table", required = true) @PathVariable(value = "id") Long id) {
        return authorService.deleteAuthor(id);
    }
}

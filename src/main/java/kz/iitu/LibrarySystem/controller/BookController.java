package kz.iitu.LibrarySystem.controller;
import io.swagger.annotations.*;
import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.entity.Book;
import kz.iitu.LibrarySystem.entity.Status;
import kz.iitu.LibrarySystem.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@Api(value="Book controller class", description = "Operations pertaining to book in Library System")
public class BookController {

    @Autowired
    private BookService bookService;

    @ApiOperation(value = "Add a book")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Book successfully stored in database table"),
            @ApiResponse(code = 401, message = "You are not authorized to add a new book"),
            @ApiResponse(code = 403, message = "Accessing to add a new book is forbidden"),
            @ApiResponse(code = 404, message = "The book is not found to add in database")
    }
    )
    @PostMapping("/admin/create/book")
    public void createBook(
            @ApiParam(value = "Book object store in database table", required = true) @Valid @RequestBody Book book)  {
        bookService.createBook(book);
    }

    @ApiOperation(value = "View a list of all books", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of all books"),
            @ApiResponse(code = 401, message = "You are not authorized to view the list of all books"),
            @ApiResponse(code = 403, message = "Accessing the list of all books is forbidden"),
            @ApiResponse(code = 404, message = "The list of all books is not found")
    }
    )
    @GetMapping("/books")
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @ApiOperation(value = "Get a book by status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find book by status"),
            @ApiResponse(code = 401, message = "You are not authorized to find book by status"),
            @ApiResponse(code = 403, message = "Finding book by status is forbidden"),
            @ApiResponse(code = 404, message = "Finding book by status is not found")
    }
    )
    @GetMapping("/books/{status}")
    public List<Book> showBookByStatus(
            @ApiParam(value = "Book status from which book object will retrieve", required = true) @PathVariable(value = "status") Status status) {
        return bookService.showBookByStatus(status);
    }

    @ApiOperation(value = "Get a book by author")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find book by author"),
            @ApiResponse(code = 401, message = "You are not authorized to find book by author"),
            @ApiResponse(code = 403, message = "Finding book by author is forbidden"),
            @ApiResponse(code = 404, message = "Finding book by author is not found")
    }
    )
    @GetMapping("books/{author}")
    public List<Book> findBookByAuthor(
            @ApiParam(value = "Book author from which book object will retrieve", required = true) @PathVariable(value = "author") Author author) {
        return bookService.findBookByAuthor(author);
    }

    @ApiOperation(value = "Get a book by description")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find book by description"),
            @ApiResponse(code = 401, message = "You are not authorized to find book by description"),
            @ApiResponse(code = 403, message = "Finding book by description is forbidden"),
            @ApiResponse(code = 404, message = "Finding book by description is not found")
    }
    )
    @GetMapping("/books/{description}")
    public Book showBookByDescription(
            @ApiParam(value = "Book description from which book object will retrieve", required = true) @PathVariable(value = "description") String description) {
        return bookService.showBookByDescription(description);
    }

    @ApiOperation(value = "Get a book by name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find book by name"),
            @ApiResponse(code = 401, message = "You are not authorized to find book by name"),
            @ApiResponse(code = 403, message = "Finding book by name is forbidden"),
            @ApiResponse(code = 404, message = "Finding book by name is not found")
    }
    )
    @GetMapping("/books/{name}")
    public Book showBookByName(
            @ApiParam(value = "Book name from which book object will retrieve", required = true) @PathVariable(value = "name") String name) {
        return bookService.showBookByName(name);
    }

    @ApiOperation(value = "Get a book by Id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find book by id"),
            @ApiResponse(code = 401, message = "You are not authorized to find book by id"),
            @ApiResponse(code = 403, message = "Finding book by id is forbidden"),
            @ApiResponse(code = 404, message = "Finding book by id is not found")
    }
    )
    @GetMapping("/find/books/{id}")
    public Book showBookById(
            @ApiParam(value = "Book id from which book object will retrieve", required = true) @PathVariable(value = "id") Long id) {
        return bookService.showBookById(id);
    }

    @ApiOperation(value = "Update a book quantity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated quantity of book"),
            @ApiResponse(code = 401, message = "You are not authorized to update book quantity"),
            @ApiResponse(code = 403, message = "Update quantity of book is forbidden"),
            @ApiResponse(code = 404, message = "Update quantity of book is not found")
    }
    )
    @PatchMapping("/admin/update/books/quantity/{isbn}")
    public void updateBookQuantity(
            @ApiParam(value = "Book isbn to update book object", required = true) @PathVariable(value = "isbn") String isbn,
            @ApiParam(value = "Update book object", required = true) @Valid @RequestBody int quantity){
        bookService.updateBookQuantity(isbn, quantity);
        System.out.println("Book was updated!");
    }

    @ApiOperation(value = "Update a book status")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated status of book"),
            @ApiResponse(code = 401, message = "You are not authorized to update book status"),
            @ApiResponse(code = 403, message = "Update status of book is forbidden"),
            @ApiResponse(code = 404, message = "Update status of book is not found")
    }
    )
    @PatchMapping("/admin/update/books/status/{isbn}")
    public void updateBookStatus(
            @ApiParam(value = "Book isbn to update book object", required = true) @PathVariable(value = "isbn") String isbn,
            @ApiParam(value = "Update book object", required = true) @Valid @RequestBody Status status) {
        bookService.updateBookStatus(isbn, status);
        System.out.println("Book was updated!");
    }

    @ApiOperation(value = "Get a book by isbn")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find book by isbn"),
            @ApiResponse(code = 401, message = "You are not authorized to find book by isbn"),
            @ApiResponse(code = 403, message = "Finding book by isbn is forbidden"),
            @ApiResponse(code = 404, message = "Finding book by isbn is not found")
    }
    )
    @GetMapping("/books/{isbn}")
    public Book showBookByIsbn(
            @ApiParam(value = "Book isbn from which book object will retrieve", required = true) @PathVariable(value = "isbn") String isbn) {
        return bookService.showBookByIsbn(isbn);
    }

    @ApiOperation(value = "Delete a book")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Book successfully deleted from table"),
            @ApiResponse(code = 401, message = "You are not authorized to delete book"),
            @ApiResponse(code = 403, message = "Accessing to delete book is forbidden"),
            @ApiResponse(code = 404, message = "The book is not found to delete")
    }
    )
    @DeleteMapping("/admin/delete/books/{id}")
    public Map<String, Boolean> deleteBook(
            @ApiParam(value = "Book Id from which book object will delete from database table", required = true) @PathVariable(value = "id") Long id) {
        return bookService.deleteBook(id);
    }

}


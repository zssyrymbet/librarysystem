package kz.iitu.LibrarySystem.controller;
import io.swagger.annotations.*;
import kz.iitu.LibrarySystem.entity.LibraryReader;
import kz.iitu.LibrarySystem.service.LibraryReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Map;

@RestController
@Api(value="Library reader controller class", description = "Operations pertaining to library reader in Library System")
public class LibraryReaderController {
    @Autowired
    private LibraryReaderService libraryReaderService;

    @ApiOperation(value = "Add a library reader")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Library reader successfully stored in database table"),
            @ApiResponse(code = 401, message = "You are not authorized to add a new library reader"),
            @ApiResponse(code = 403, message = "Accessing to add a new library reader is forbidden"),
            @ApiResponse(code = 404, message = "The library reader is not found to add in database")
    }
    )
    @PostMapping("/admin/create/library_reader")
    public void createLibraryReader(
            @ApiParam(value = "Library Reader object store in database table", required = true) @Valid @RequestBody LibraryReader libraryReader)  {
        libraryReaderService.createLibraryReader(libraryReader);
    }

    @ApiOperation(value = "Get a library reader by name and surname")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find library reader by name and surname"),
            @ApiResponse(code = 401, message = "You are not authorized to find library reader by name and suranem"),
            @ApiResponse(code = 403, message = "Finding library reader by name and surname is forbidden"),
            @ApiResponse(code = 404, message = "Finding library reader by name and surname is not found")
    }
    )
    @PatchMapping("/find/library_readers/find/")
    public LibraryReader getLibraryReader(
            @ApiParam(value = "Library Reader name from which library reader object will retrieve", required = true) @RequestParam("name") String name,
            @ApiParam(value = "Library Reader surname from which library reader object will retrieve", required = true) @RequestParam("surname") String surname) {
        return libraryReaderService.getLibraryReader(name, surname);
    }

    @ApiOperation(value = "Get a library reader by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully find library reader by id"),
            @ApiResponse(code = 401, message = "You are not authorized to find library reader by id"),
            @ApiResponse(code = 403, message = "Finding library reader by id is forbidden"),
            @ApiResponse(code = 404, message = "Finding library reader by id is not found")
    }
    )
    @GetMapping("/find/library_readers/{id}")
    public LibraryReader showLibraryReaderById(
            @ApiParam(value = "Library Reader id from which library reader object will retrieve", required = true) @PathVariable(value = "id") Long id) {
        return libraryReaderService.showLibraryReaderById(id);
    }

    @ApiOperation(value = "Delete a library reader")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Library reader successfully deleted from table"),
            @ApiResponse(code = 401, message = "You are not authorized to delete library reader"),
            @ApiResponse(code = 403, message = "Accessing to delete library reader is forbidden"),
            @ApiResponse(code = 404, message = "The library reader is not found to delete")
    }
    )
    @DeleteMapping("/admin/delete/library_readers/{id}")
    public Map<String, Boolean> deleteLibraryReader(
            @ApiParam(value = "Library Reader id from which library reader object will delete from database table", required = true) @PathVariable(value = "id") Long id) {
        return libraryReaderService.deleteLibraryReader(id);
    }
}


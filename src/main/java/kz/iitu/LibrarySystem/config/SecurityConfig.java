package kz.iitu.LibrarySystem.config;
import kz.iitu.LibrarySystem.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserServiceImpl userService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()

                    //ALL
                    .antMatchers("/auth/**").permitAll()
                    .antMatchers("/welcome/**").permitAll()
                    .antMatchers("/user/registration/**").permitAll()
                    .antMatchers("/authors/**").permitAll()
                    .antMatchers("/books/**").permitAll()
                    .antMatchers("/genres/**").permitAll()
                    .antMatchers("/*", "/v2/api-docs", "/configuration/", "/swagger/*", "/webjars/*").permitAll()

                    //ROLE_ADMIN
                    .antMatchers("/admin/create/**").hasRole("ADMIN")
                    .antMatchers("/admin/delete/**").hasRole("ADMIN")
                    .antMatchers("/admin/update/quantity/**").hasRole("ADMIN")
                    .antMatchers("/admin/update/status/**").hasRole("ADMIN")
                    .antMatchers("/admin/update/update/**").hasRole("ADMIN")
                    .antMatchers("/find/authors/**").hasRole("ADMIN")
                    .antMatchers("/find/users/**").hasRole("ADMIN")
                    .antMatchers("/find/books/**").hasRole("ADMIN")
                    .antMatchers("/find/genres/**").hasRole("ADMIN")
                    .antMatchers("/find/librarians/**").hasRole("ADMIN")
                    .antMatchers("/find/library_readers/**").hasRole("ADMIN")


                    //ROLE_READER
                    .antMatchers("/reader/borrowed_books/**").hasRole("READER")

                    //ROLE_LIBRARIAN
                    .antMatchers("/librarian/borrowed_books/**").hasRole("LIBRARIAN")
                    .antMatchers("/librarian/create/**").hasRole("LIBRARIAN")
                    .antMatchers("/find/authors/**").hasRole("LIBRARIAN")
                    .antMatchers("/find/users/**").hasRole("LIBRARIAN")
                    .antMatchers("/find/books/**").hasRole("LIBRARIAN")
                    .antMatchers("/find/genres/**").hasRole("LIBRARIAN")
                    .antMatchers("/find/librarians/**").hasRole("LIBRARIAN")
                    .antMatchers("/find/library_readers/**").hasRole("LIBRARIAN")

                    .anyRequest().authenticated()
                    .and()
                    .addFilter(new JwtTokenGeneratorFilter(authenticationManager()))
                    .addFilterAfter(new JwtTokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }
    
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security", "/swagger-ui.html", "/webjars/**");
    }
}
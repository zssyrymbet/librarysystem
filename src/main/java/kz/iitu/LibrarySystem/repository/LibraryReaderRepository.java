package kz.iitu.LibrarySystem.repository;
import kz.iitu.LibrarySystem.entity.LibraryReader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibraryReaderRepository extends JpaRepository<LibraryReader, Long> {
    LibraryReader findLibraryReaderByUsernameAndSurname(String username, String surname);
    LibraryReader findLibraryReaderById(Long id);
}

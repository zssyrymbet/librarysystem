package kz.iitu.LibrarySystem.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "librarians")
@ApiModel(description = "All details about the Genre")
public class Librarian extends User{
    @ApiModelProperty(notes = "The user email")
    private String email;

    @ApiModelProperty(notes = "The user number")
    private int phoneNumber;
}

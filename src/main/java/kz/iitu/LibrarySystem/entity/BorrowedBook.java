package kz.iitu.LibrarySystem.entity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "borrowedBooks")
@ApiModel(description = "All details about the Book")
public class BorrowedBook {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated borrowed book ID")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "book_id", updatable = false)
    @ApiModelProperty(notes = "The borrowed book ID in borrowed book table")
    private Book book;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", updatable = false)
    @JsonIgnoreProperties("borrowedBooks")
    @ApiModelProperty(notes = "The borrowed user ID in borrowed book table")
    private LibraryReader libraryReader;

    @ApiModelProperty(notes = "The borrowed book issue date")
    private Date issue_date;

    @ApiModelProperty(notes = "The borrowed book return date")
    private Date return_date;

    @ApiModelProperty(notes = "The borrowed book status")
    private Status status;
}

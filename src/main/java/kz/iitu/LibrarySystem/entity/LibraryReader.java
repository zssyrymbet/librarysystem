package kz.iitu.LibrarySystem.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "libraryReaders")
@ApiModel(description = "All details about the Genre")
public class LibraryReader extends User{
    @ApiModelProperty(notes = "The library reader phone number")
    private int phoneNumber;

    @OneToMany(mappedBy = "libraryReader", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "The list of borrowed books by this user")
    private Set<BorrowedBook> borrowedBooks;
}

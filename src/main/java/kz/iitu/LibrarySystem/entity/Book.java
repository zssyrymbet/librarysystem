package kz.iitu.LibrarySystem.entity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "books")
@ApiModel(description = "All details about the Book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated book ID")
    private Long id;

    @ApiModelProperty(notes = "The book name")
    private String name;

    @Column(unique = true)
    @ApiModelProperty(notes = "The book ISBN")
    private String isbn;

    @ApiModelProperty(notes = "The book description")
    private String description;

    @ApiModelProperty(notes = "The book quantity")
    private int quantity;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "author_id", updatable = false)
    @JsonIgnoreProperties("books")
    @ApiModelProperty(notes = "The book author")
    private Author author;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "book_genres",
            joinColumns = {@JoinColumn(name = "book_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id", referencedColumnName = "id")}
    )
    @JsonIgnoreProperties("book")
    @ApiModelProperty(notes = "The list of genres by this book")
    private Set<Genre> genres;

    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "The book status")
    private Status status;

    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    @ApiModelProperty(notes = "The list of borrowed books by this book")
    private Set<BorrowedBook> borrowedBooks;
}

package kz.iitu.LibrarySystem.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "authors")
@ApiModel(description = "All details about the Author")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated author ID")
    private Long id;

    @ApiModelProperty(notes = "The author name")
    private String name;

    @ApiModelProperty(notes = "The author surname")
    private String surname;

    @ApiModelProperty(notes = "The author biography")
    private String biography;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ApiModelProperty(notes = "The list of books by this author")
    private Set<Book> books;
}

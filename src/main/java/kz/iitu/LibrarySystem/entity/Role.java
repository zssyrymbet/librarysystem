package kz.iitu.LibrarySystem.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "roles")
@ApiModel(description = "All details about the Role")
public class Role implements GrantedAuthority {

    //Roles: ADMIN, READER, LIBRARIAN
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated role ID")
    private Long id;

    @ApiModelProperty(notes = "The role number")
    private String name;

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    @ApiModelProperty(notes = "The list of users by this roles")
    private List<User> user;

    @Override
    public String getAuthority() {
        return name;
    }
}

package kz.iitu.LibrarySystem.entity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "genres")
@ApiModel(description = "All details about the Genre")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated genre ID")
    private Long id;

    @ApiModelProperty(notes = "The genre name")
    private String name;

    @ApiModelProperty(notes = "The genre description")
    private String description;

    @ManyToMany(mappedBy = "genres", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("genres")
    @ApiModelProperty(notes = "The list of book by this genre")
    private List<Book> book;
}

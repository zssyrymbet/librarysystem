package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.entity.Book;
import kz.iitu.LibrarySystem.entity.Status;
import java.util.List;
import java.util.Map;

public interface BookService {
    void createBook(Book book);
    List<Book> getAllBooks();
    List<Book> showBookByStatus(Status status);
    List<Book> findBookByAuthor(Author author);
    Book showBookByDescription(String description);
    Book showBookByName(String name);
    Book showBookById(Long id);
    Book showBookByIsbn(String isbn);
    void updateBookQuantity(String isbn, int quantity);
    void updateBookStatus(String isbn, Status status);
    Map<String, Boolean> deleteBook(Long id);
}

package kz.iitu.LibrarySystem.service.impl;
import lombok.NonNull;
import kz.iitu.LibrarySystem.entity.BorrowedBook;
import kz.iitu.LibrarySystem.entity.LibraryReader;
import kz.iitu.LibrarySystem.entity.Status;
import kz.iitu.LibrarySystem.repository.BorrowedBookRepository;
import kz.iitu.LibrarySystem.service.BorrowedBookService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BorrowedBookServiceImpl implements BorrowedBookService {

    @Autowired
    private BorrowedBookRepository bookRepository;

    @Override
    public void createBook(BorrowedBook borrowedBook) {
        bookRepository.saveAndFlush(borrowedBook);
    }

    @Override
    public List<BorrowedBook> getAllBorrowedBooks(LibraryReader libraryReader) {
        return bookRepository.findBookByLibraryReader(libraryReader);
    }

    @Override
    public List<BorrowedBook> findAllByStatus(Status status) {
        return bookRepository.findAllByStatus(status);
    }

    @Override
    public BorrowedBook getBookById(Long id) {
        return bookRepository.findBookById(id);
    }

    @Override
    public void updateBook(Long id, Status status) {
        @NonNull BorrowedBook book = bookRepository.findBookById(id);
            book.setStatus(status);
            bookRepository.saveAndFlush(book);
    }

    @Override
    public Map<String, Boolean> deleteBorrowedBook(Long id) {
        bookRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

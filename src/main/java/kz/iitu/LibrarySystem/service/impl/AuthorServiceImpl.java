package kz.iitu.LibrarySystem.service.impl;
import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.repository.AuthorRepository;
import kz.iitu.LibrarySystem.service.AuthorService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public void createAuthor(Author author) {
        authorRepository.saveAndFlush(author);
    }

    @Override
    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    @Override
    public Author getAuthor(String name, String surname) {
        return authorRepository.findAuthorByNameAndSurname(name, surname);
    }

    @Override
    public Author getAuthorById(Long id) {
        return authorRepository.findAuthorById(id);
    }

    @Override
    public Map<String, Boolean> deleteAuthor(Long id) {
        authorRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

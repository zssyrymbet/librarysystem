package kz.iitu.LibrarySystem.service.impl;
import kz.iitu.LibrarySystem.entity.LibraryReader;
import kz.iitu.LibrarySystem.repository.LibraryReaderRepository;
import kz.iitu.LibrarySystem.service.LibraryReaderService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service
public class LibraryReaderServiceImpl implements LibraryReaderService {

    @Autowired
    private LibraryReaderRepository libraryReaderRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    public void createLibraryReader(LibraryReader libraryReader) {
        libraryReader.setPassword(passwordEncoder.encode(libraryReader.getPassword()));
        libraryReaderRepository.saveAndFlush(libraryReader);
    }

    @Override
    public LibraryReader getLibraryReader(String username, String surname) {
        return libraryReaderRepository.findLibraryReaderByUsernameAndSurname(username, surname);
    }

    @Override
    public LibraryReader showLibraryReaderById(Long id) {
        return libraryReaderRepository.findLibraryReaderById(id);
    }

    @Override
    public Map<String, Boolean> deleteLibraryReader(Long id) {
        libraryReaderRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

package kz.iitu.LibrarySystem.service.impl;
import kz.iitu.LibrarySystem.entity.Genre;
import kz.iitu.LibrarySystem.repository.GenreRepository;
import kz.iitu.LibrarySystem.service.GenreService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GenreServiceImpl implements GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Override
    public void createGenre(Genre genre) {
        genreRepository.saveAndFlush(genre);
    }

    @Override
    public List<Genre> getAllGenres() {
        return genreRepository.findAll();
    }

    @Override
    public Genre showGenreById(Long id) {
        return genreRepository.findGenreById(id);
    }

    @Override
    public Map<String, Boolean> deleteGenre(Long id) {
        genreRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

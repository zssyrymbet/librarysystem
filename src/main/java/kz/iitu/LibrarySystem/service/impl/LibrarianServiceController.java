package kz.iitu.LibrarySystem.service.impl;
import kz.iitu.LibrarySystem.entity.Librarian;
import kz.iitu.LibrarySystem.repository.LibrarianRepository;
import kz.iitu.LibrarySystem.service.LibrarianService;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LibrarianServiceController implements LibrarianService {

    @Autowired
    private LibrarianRepository librarianRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public void createLibrarian(Librarian librarian) {
        librarian.setPassword(passwordEncoder.encode(librarian.getPassword()));
        librarianRepository.saveAndFlush(librarian);
    }

    @Override
    public List<Librarian> getAllLibrarians() {
        return librarianRepository.findAll();
    }

    @Override
    public Map<String, Boolean> deleteLibrarian(Long id) {
        librarianRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

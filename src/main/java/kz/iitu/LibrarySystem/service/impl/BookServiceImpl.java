package kz.iitu.LibrarySystem.service.impl;
import kz.iitu.LibrarySystem.entity.Author;
import kz.iitu.LibrarySystem.entity.Book;
import kz.iitu.LibrarySystem.entity.Status;
import kz.iitu.LibrarySystem.repository.BookRepository;
import kz.iitu.LibrarySystem.service.BookService;
import lombok.NonNull;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void createBook(Book book) {
        bookRepository.saveAndFlush(book);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public List<Book> showBookByStatus(Status status) {
        return bookRepository.findAllByStatus(status);
    }

    @Override
    public List<Book> findBookByAuthor(Author author) {
        return bookRepository.findBookByAuthor(author);
    }

    @Override
    public Book showBookByDescription(String description) {
        return bookRepository.findBookByDescription(description);
    }

    @Override
    public Book showBookByName(String name) {
        return bookRepository.findBookByName(name);
    }

    @Override
    public Book showBookById(Long id) {
        return bookRepository.findBookById(id);
    }

    @Override
    public Book showBookByIsbn(String isbn) {
        return bookRepository.findBookByIsbn(isbn);
    }

    @Override
    public void updateBookQuantity(String isbn, int quantity) {
        @NonNull Book book = bookRepository.findBookByIsbn(isbn);
            book.setQuantity(quantity);
            bookRepository.saveAndFlush(book);
    }

    @Override
    public void updateBookStatus(String isbn, Status status) {
        @NonNull Book book = bookRepository.findBookByIsbn(isbn);
            book.setStatus(status);
            bookRepository.saveAndFlush(book);
    }

    @Override
    public Map<String, Boolean> deleteBook(Long id) {
        bookRepository.deleteById(id);
        val response = new HashMap<String, Boolean>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}

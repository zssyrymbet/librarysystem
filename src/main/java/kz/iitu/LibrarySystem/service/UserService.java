package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.User;
import java.util.List;
import java.util.Map;

public interface UserService {
    List<User> getAllUsers();
    void createUser(User user);
    User findById(Long id);
    User findByUsername(String username);
    void updateUser(Long id, User user);
    Map<String, Boolean> deleteUser(Long id);
}

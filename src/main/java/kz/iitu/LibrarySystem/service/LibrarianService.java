package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.Librarian;
import java.util.List;
import java.util.Map;

public interface LibrarianService {
    void createLibrarian(Librarian librarian);
    List<Librarian> getAllLibrarians();
    Map<String, Boolean> deleteLibrarian(Long id);
}

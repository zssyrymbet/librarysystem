package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.BorrowedBook;
import kz.iitu.LibrarySystem.entity.LibraryReader;
import kz.iitu.LibrarySystem.entity.Status;
import java.util.List;
import java.util.Map;

public interface BorrowedBookService {
    void createBook(BorrowedBook borrowedBook);
    List<BorrowedBook> getAllBorrowedBooks(LibraryReader libraryReader);
    List<BorrowedBook> findAllByStatus(Status status);
    BorrowedBook getBookById(Long id);
    void updateBook(Long id, Status status);
    Map<String, Boolean> deleteBorrowedBook(Long id);

}

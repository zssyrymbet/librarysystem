package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.Genre;
import java.util.List;
import java.util.Map;

public interface GenreService {
    void createGenre(Genre genre);
    List<Genre> getAllGenres();
    Genre showGenreById(Long id);
    Map<String, Boolean> deleteGenre(Long id);
}

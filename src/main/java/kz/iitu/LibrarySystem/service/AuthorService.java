package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.Author;
import java.util.List;
import java.util.Map;

public interface AuthorService {
    void createAuthor(Author author);
    List<Author> getAllAuthors();
    Author getAuthor(String name, String surname);
    Author getAuthorById(Long id);
    Map<String, Boolean> deleteAuthor(Long id);
}

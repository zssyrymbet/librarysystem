package kz.iitu.LibrarySystem.service;
import kz.iitu.LibrarySystem.entity.LibraryReader;
import java.util.Map;

public interface LibraryReaderService {
    void createLibraryReader(LibraryReader libraryReader);
    LibraryReader getLibraryReader(String username, String surname);
    LibraryReader showLibraryReaderById(Long id);
    Map<String, Boolean> deleteLibraryReader(Long id);
}
